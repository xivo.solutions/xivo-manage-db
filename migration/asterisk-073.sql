/*
 * XiVO Base-Config
 * Copyright (C) 2012-2014  Avencall
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

BEGIN;

DELETE FROM "agentglobalparams" WHERE "option_name" = 'endcall';

DELETE FROM "agentglobalparams" WHERE "option_name" = 'maxlogintries';
INSERT INTO "agentglobalparams" VALUES (DEFAULT,'agents','maxlogintries','3');

DELETE FROM "agentglobalparams" WHERE "option_name" = 'custom_beep';

DELETE FROM "agentglobalparams" WHERE "option_name" = 'goodbye';

DELETE FROM "agentglobalparams" WHERE "option_name" = 'recordagentcalls';

DELETE FROM "agentglobalparams" WHERE "option_name" = 'recordformat';

DELETE FROM "agentglobalparams" WHERE "option_name" = 'urlprefix';

DELETE FROM "agentglobalparams" WHERE "option_name" = 'savecallsin';

COMMIT;
