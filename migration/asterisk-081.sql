/*
 * XiVO Base-Config
 * Copyright (C) 2012-2014  Avencall
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

BEGIN;

DROP TABLE IF EXISTS "agent_login_status";
CREATE TABLE "agent_login_status" (
    "agent_id"        INTEGER      PRIMARY KEY,
    "interface"       VARCHAR(128) NOT NULL  UNIQUE,
    "login_at"        TIMESTAMP    NOT NULL  DEFAULT NOW()
);

GRANT ALL ON "agent_login_status" TO "asterisk";

COMMIT;
