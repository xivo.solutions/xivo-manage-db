"""Add all_mediaserver view

Revision ID: 335f0d173f38
Revises: d65f39f4f72

"""

# revision identifiers, used by Alembic.
revision = '335f0d173f38'
down_revision = 'd65f39f4f72'

from alembic import op


def upgrade():
    op.execute("""
      CREATE OR REPLACE VIEW all_mediaserver AS
        (SELECT 0 as id, 'default' as name, 'MDS Main' as display_name, netiface.address as voip_ip, true as read_only
        FROM netiface where netiface.networktype='voip' LIMIT 1)
      UNION ALL
        (SELECT id, name, display_name, voip_ip, false as read_only
        FROM mediaserver);
    """)


def downgrade():
    op.drop_view('all_mediaserver')
