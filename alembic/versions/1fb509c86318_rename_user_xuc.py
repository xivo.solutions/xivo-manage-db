"""rename user xuc

Revision ID: 1fb509c86318
Revises: 33190b4b4d21

"""

# revision identifiers, used by Alembic.
revision = '1fb509c86318'
down_revision = '33190b4b4d21'

from alembic import op


def upgrade():
    op.execute("""
        UPDATE "func_key_template"
        SET (name, private) = ('xuc technical', 'true')
        WHERE id = (SELECT func_key_private_template_id FROM userfeatures WHERE loginclient = 'xuc')
    """)
    op.execute("""
        UPDATE "userfeatures"
        SET (firstname, lastname) = ('xuc', 'technical')
        WHERE loginclient = 'xuc'
    """)


def downgrade():
    pass
