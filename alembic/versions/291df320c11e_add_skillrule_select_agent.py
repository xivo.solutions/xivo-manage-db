"""add skillrule select_agent

Revision ID: 291df320c11e
Revises: 1fb509c86318

"""

# revision identifiers, used by Alembic.
revision = '291df320c11e'
down_revision = '1fb509c86318'

from alembic import op


def upgrade():
    op.execute("""
        INSERT INTO "queueskillrule"
            (name, rule)
        SELECT
            'select_agent', '$agent > 0'
        WHERE NOT EXISTS
            (SELECT 1 FROM "queueskillrule" where name = 'select_agent')
    """)
    pass


def downgrade():
    pass
