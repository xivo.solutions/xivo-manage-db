"""Set enablexfer default

Revision ID: 2d9ffeaee224
Revises: 291df320c11e

"""

# revision identifiers, used by Alembic.
revision = '2d9ffeaee224'
down_revision = '291df320c11e'

from alembic import op


def upgrade():
    op.alter_column('userfeatures', 'enablexfer', server_default='0')


def downgrade():
    op.alter_column('userfeatures', 'enablexfer', server_default='1')
