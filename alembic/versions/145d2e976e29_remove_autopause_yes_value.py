"""remove autopause yes value

Revision ID: 145d2e976e29
Revises: 5381b673cf2c

"""

# revision identifiers, used by Alembic.
revision = '145d2e976e29'
down_revision = '5381b673cf2c'

from alembic import op


def upgrade():
    op.execute("UPDATE queue SET autopause = 'all' WHERE autopause = 'yes'")
    op.execute("ALTER TABLE queue DROP CONSTRAINT IF EXISTS queue_autopause_check")
    op.execute("ALTER TABLE queue ADD CONSTRAINT queue_autopause_check CHECK (autopause IN ('no','all'))")


def downgrade():
    op.execute("ALTER TABLE queue DROP CONSTRAINT IF EXISTS queue_autopause_check")
    op.execute("ALTER TABLE queue ADD CONSTRAINT queue_autopause_check CHECK (autopause IN ('no','yes','all'))")
