"""add privileges to configmgt user on queuefeatures

Revision ID: 350ab2b7b068
Revises: abbd6671133

"""

# revision identifiers, used by Alembic.
revision = '350ab2b7b068'
down_revision = 'abbd6671133'

from alembic import op


def upgrade():
    op.execute("GRANT ALL PRIVILEGES ON queuefeatures TO configmgt")


def downgrade():
    op.execute("REVOKE ALL PRIVILEGES ON queuefeatures FROM configmgt")
    op.execute("GRANT SELECT ON queuefeatures TO configmgt")
