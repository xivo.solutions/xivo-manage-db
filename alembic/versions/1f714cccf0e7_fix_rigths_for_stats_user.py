"""Fix rigths for stats user

Revision ID: 1f714cccf0e7
Revises: 350ab2b7b068

"""

# revision identifiers, used by Alembic.
revision = '1f714cccf0e7'
down_revision = '350ab2b7b068'

from alembic import op

DB_OBJECTS = [
    'qualifications',
    'subqualifications',
    'queue_qualification',
    'qualification_answers',
    'qualifications_id_seq',
    'subqualifications_id_seq',
    'queue_qualification_id_seq',
    'qualification_answers_id_seq',
]


def upgrade():
    for obj in DB_OBJECTS:
        op.execute('REVOKE ALL PRIVILEGES ON TABLE ' + obj + ' FROM stats')
        op.execute('GRANT SELECT ON TABLE ' + obj + ' TO stats')


def downgrade():
    for obj in DB_OBJECTS:
        op.execute('GRANT ALL PRIVILEGES ON TABLE ' + obj + ' TO stats')
