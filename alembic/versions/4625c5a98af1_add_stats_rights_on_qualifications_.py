"""Add stats rights on qualifications tables

Revision ID: 4625c5a98af1
Revises: 581e85327dfa

"""

# revision identifiers, used by Alembic.
revision = '4625c5a98af1'
down_revision = '581e85327dfa'

from alembic import op


def upgrade():
    op.execute('GRANT ALL PRIVILEGES ON TABLE qualifications TO stats')
    op.execute('GRANT ALL PRIVILEGES ON TABLE subqualifications TO stats')
    op.execute('GRANT ALL PRIVILEGES ON TABLE queue_qualification TO stats')
    op.execute('GRANT ALL PRIVILEGES ON TABLE qualification_answers TO stats')
    op.execute('GRANT ALL ON qualification_answers_id_seq  to stats')
    op.execute('GRANT ALL ON qualifications_id_seq to stats')
    op.execute('GRANT ALL ON subqualifications_id_seq to stats')
    op.execute('GRANT ALL ON queue_qualification_id_seq to stats')
    pass


def downgrade():
    pass
