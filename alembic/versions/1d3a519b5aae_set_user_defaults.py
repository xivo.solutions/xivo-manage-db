"""set_user_defaults

Revision ID: 1d3a519b5aae
Revises: 489961188626

"""

# revision identifiers, used by Alembic.
revision = '1d3a519b5aae'
down_revision = '489961188626'

from alembic import op


def upgrade():
    op.alter_column("userfeatures", "ringseconds", server_default="20")
    op.alter_column("userfeatures", "simultcalls", server_default="2")


def downgrade():
    op.alter_column("userfeatures", "ringseconds", server_default="30")
    op.alter_column("userfeatures", "simultcalls", server_default="5")
