"""${message}

Revision ID: ${up_revision}
Revises: ${down_revision}

"""

# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}

from alembic import op
import sqlalchemy as sa
${imports if imports else ""}

def upgrade():
    # Note: If you create a table using sqlalchemy command:
    #  - command will be run as asterisk,
    #  - thereofre the owner's object will be asterisk
    #  - and therefore postgresql will give stats user the SELECT privilege
    ${upgrades if upgrades else "pass"}


def downgrade():
    ${downgrades if downgrades else "pass"}
