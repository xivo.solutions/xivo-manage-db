xivo-manage-db
==============

Old repositories containing database migration scripts. They are now included in xivo-db repository.


Running unit tests
------------------

```
ci-tox
```
